﻿using StudentKeeping.Presentation.test;

namespace StudentKeeping.Presentation.Students
{
    partial class StudentList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.PasirinktasStudentas = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IsDeletedHide = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Vardas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pavardė = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TelNr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Klasė = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KlasėsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Atnaujinti = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Ištrinti = new System.Windows.Forms.DataGridViewButtonColumn();
            this.StudentoNaudotojoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.checkRodytiVisus = new System.Windows.Forms.CheckBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewButtonColumn2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.txtAdresas = new System.Windows.Forms.TextBox();
            this.txtTelNr = new System.Windows.Forms.TextBox();
            this.txtPavardė = new System.Windows.Forms.TextBox();
            this.txtVardas = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtKlasė = new System.Windows.Forms.TextBox();
            this.chkboxPažymėtiVisus = new System.Windows.Forms.CheckBox();
            this.btnPasirinkti = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PasirinktasStudentas,
            this.Id,
            this.IsDeletedHide,
            this.Vardas,
            this.Pavardė,
            this.TelNr,
            this.Adresas,
            this.Klasė,
            this.KlasėsId,
            this.Atnaujinti,
            this.Ištrinti,
            this.StudentoNaudotojoId});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dataGridView1.Location = new System.Drawing.Point(0, 92);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(788, 270);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // PasirinktasStudentas
            // 
            this.PasirinktasStudentas.FalseValue = "false";
            this.PasirinktasStudentas.HeaderText = "";
            this.PasirinktasStudentas.Name = "PasirinktasStudentas";
            this.PasirinktasStudentas.ReadOnly = true;
            this.PasirinktasStudentas.TrueValue = "true";
            this.PasirinktasStudentas.Width = 35;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Visible = false;
            // 
            // IsDeletedHide
            // 
            this.IsDeletedHide.HeaderText = "IsDeletedHide";
            this.IsDeletedHide.Name = "IsDeletedHide";
            this.IsDeletedHide.ReadOnly = true;
            this.IsDeletedHide.Visible = false;
            // 
            // Vardas
            // 
            this.Vardas.HeaderText = "Vardas";
            this.Vardas.Name = "Vardas";
            this.Vardas.ReadOnly = true;
            // 
            // Pavardė
            // 
            this.Pavardė.HeaderText = "Pavardė";
            this.Pavardė.Name = "Pavardė";
            this.Pavardė.ReadOnly = true;
            // 
            // TelNr
            // 
            this.TelNr.HeaderText = "Tel. Nr.";
            this.TelNr.Name = "TelNr";
            this.TelNr.ReadOnly = true;
            // 
            // Adresas
            // 
            this.Adresas.HeaderText = "Adresas";
            this.Adresas.Name = "Adresas";
            this.Adresas.ReadOnly = true;
            this.Adresas.Width = 150;
            // 
            // Klasė
            // 
            this.Klasė.HeaderText = "Klasė";
            this.Klasė.Name = "Klasė";
            this.Klasė.ReadOnly = true;
            // 
            // KlasėsId
            // 
            this.KlasėsId.HeaderText = "";
            this.KlasėsId.Name = "KlasėsId";
            this.KlasėsId.ReadOnly = true;
            this.KlasėsId.Visible = false;
            // 
            // Atnaujinti
            // 
            this.Atnaujinti.HeaderText = "";
            this.Atnaujinti.Name = "Atnaujinti";
            this.Atnaujinti.ReadOnly = true;
            this.Atnaujinti.Text = "Atnaujinti";
            this.Atnaujinti.UseColumnTextForButtonValue = true;
            this.Atnaujinti.Width = 80;
            // 
            // Ištrinti
            // 
            this.Ištrinti.HeaderText = "";
            this.Ištrinti.Name = "Ištrinti";
            this.Ištrinti.ReadOnly = true;
            this.Ištrinti.Text = "Ištrinti";
            this.Ištrinti.UseColumnTextForButtonValue = true;
            this.Ištrinti.Width = 80;
            // 
            // StudentoNaudotojoId
            // 
            this.StudentoNaudotojoId.HeaderText = "";
            this.StudentoNaudotojoId.Name = "StudentoNaudotojoId";
            this.StudentoNaudotojoId.ReadOnly = true;
            this.StudentoNaudotojoId.Visible = false;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(662, 8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Naujas Stundentas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkRodytiVisus
            // 
            this.checkRodytiVisus.AutoSize = true;
            this.checkRodytiVisus.Location = new System.Drawing.Point(691, 3);
            this.checkRodytiVisus.Name = "checkRodytiVisus";
            this.checkRodytiVisus.Size = new System.Drawing.Size(94, 17);
            this.checkRodytiVisus.TabIndex = 2;
            this.checkRodytiVisus.Text = "Rodyti ištrintus";
            this.checkRodytiVisus.UseVisualStyleBackColor = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(710, 26);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 23);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.HeaderText = "";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Text = "Atnaujinti";
            this.dataGridViewButtonColumn1.UseColumnTextForButtonValue = true;
            this.dataGridViewButtonColumn1.Width = 80;
            // 
            // dataGridViewButtonColumn2
            // 
            this.dataGridViewButtonColumn2.HeaderText = "";
            this.dataGridViewButtonColumn2.Name = "dataGridViewButtonColumn2";
            this.dataGridViewButtonColumn2.Text = "Ištrinti";
            this.dataGridViewButtonColumn2.UseColumnTextForButtonValue = true;
            this.dataGridViewButtonColumn2.Width = 80;
            // 
            // txtAdresas
            // 
            this.txtAdresas.Location = new System.Drawing.Point(379, 29);
            this.txtAdresas.Name = "txtAdresas";
            this.txtAdresas.Size = new System.Drawing.Size(144, 20);
            this.txtAdresas.TabIndex = 11;
            // 
            // txtTelNr
            // 
            this.txtTelNr.Location = new System.Drawing.Point(279, 29);
            this.txtTelNr.Name = "txtTelNr";
            this.txtTelNr.Size = new System.Drawing.Size(94, 20);
            this.txtTelNr.TabIndex = 9;
            // 
            // txtPavardė
            // 
            this.txtPavardė.Location = new System.Drawing.Point(180, 28);
            this.txtPavardė.Name = "txtPavardė";
            this.txtPavardė.Size = new System.Drawing.Size(93, 20);
            this.txtPavardė.TabIndex = 7;
            // 
            // txtVardas
            // 
            this.txtVardas.Location = new System.Drawing.Point(80, 28);
            this.txtVardas.Name = "txtVardas";
            this.txtVardas.Size = new System.Drawing.Size(94, 20);
            this.txtVardas.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtKlasė);
            this.panel1.Controls.Add(this.chkboxPažymėtiVisus);
            this.panel1.Controls.Add(this.txtAdresas);
            this.panel1.Controls.Add(this.txtVardas);
            this.panel1.Controls.Add(this.txtPavardė);
            this.panel1.Controls.Add(this.checkRodytiVisus);
            this.panel1.Controls.Add(this.txtTelNr);
            this.panel1.Controls.Add(this.btnRefresh);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 37);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(788, 55);
            this.panel1.TabIndex = 5;
            // 
            // txtKlasė
            // 
            this.txtKlasė.Location = new System.Drawing.Point(529, 29);
            this.txtKlasė.Name = "txtKlasė";
            this.txtKlasė.Size = new System.Drawing.Size(94, 20);
            this.txtKlasė.TabIndex = 12;
            // 
            // chkboxPažymėtiVisus
            // 
            this.chkboxPažymėtiVisus.AutoSize = true;
            this.chkboxPažymėtiVisus.Location = new System.Drawing.Point(53, 35);
            this.chkboxPažymėtiVisus.Name = "chkboxPažymėtiVisus";
            this.chkboxPažymėtiVisus.Size = new System.Drawing.Size(15, 14);
            this.chkboxPažymėtiVisus.TabIndex = 0;
            this.chkboxPažymėtiVisus.UseVisualStyleBackColor = true;
            this.chkboxPažymėtiVisus.CheckedChanged += new System.EventHandler(this.chkboxPažymėtiVisus_CheckedChanged);
            // 
            // btnPasirinkti
            // 
            this.btnPasirinkti.Location = new System.Drawing.Point(12, 8);
            this.btnPasirinkti.Name = "btnPasirinkti";
            this.btnPasirinkti.Size = new System.Drawing.Size(148, 23);
            this.btnPasirinkti.TabIndex = 6;
            this.btnPasirinkti.Text = "Pasirinkti";
            this.btnPasirinkti.UseVisualStyleBackColor = true;
            this.btnPasirinkti.Click += new System.EventHandler(this.btnPasirinkti_Click);
            // 
            // StudentList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 362);
            this.Controls.Add(this.btnPasirinkti);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "StudentList";
            this.Text = "Student List";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkRodytiVisus;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn2;
        private System.Windows.Forms.TextBox txtAdresas;
        private System.Windows.Forms.TextBox txtTelNr;
        private System.Windows.Forms.TextBox txtPavardė;
        private System.Windows.Forms.TextBox txtVardas;
        private System.Windows.Forms.DataGridViewCheckBoxColumn PasirinktasStudentas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn IsDeletedHide;
        private System.Windows.Forms.DataGridViewTextBoxColumn Vardas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pavardė;
        private System.Windows.Forms.DataGridViewTextBoxColumn TelNr;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Klasė;
        private System.Windows.Forms.DataGridViewTextBoxColumn KlasėsId;
        private System.Windows.Forms.DataGridViewButtonColumn Atnaujinti;
        private System.Windows.Forms.DataGridViewButtonColumn Ištrinti;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkboxPažymėtiVisus;
        private System.Windows.Forms.TextBox txtKlasė;
        private System.Windows.Forms.DataGridViewTextBoxColumn StudentoNaudotojoId;
        private System.Windows.Forms.Button btnPasirinkti;
    }
}