﻿using StudentKeeping.Dal.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Presentation
{
    public static class Session
    {
        public static int NaudotojoId { get; private set; }

        public static string NaudotojoVardas { get; private set; }

        public static string Vardas { get; private set; }

        public static string Pavarde { get; private set; }

        public static Role Role { get; private set; }

        public static void SukurkSesija(int naudotojoId, string naudotojoVardas, string vardas, string pavarde, Role role)
        {
            NaudotojoId = naudotojoId;
            NaudotojoVardas = naudotojoVardas;
            Vardas = vardas;
            Pavarde = pavarde;
            Role = role;
        }
    }
}
