﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.ViewModels
{
    public enum PrisijungimoStatusas
    {
        NaudotojasNerastas = 1,
        NeteisingasSlaptažodis = 2,
        Prisijugta = 3
    }
}
