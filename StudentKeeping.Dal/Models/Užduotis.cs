﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentKeeping.Dal.Models
{
    public class Užduotis
    {
        public int? Id { get; set; }

        public int? DalykoId { get; set; }

        public int? MokytojoId { get; set; }

        public string MokytojoVardasPavardė { get; set; }

        public string Pavadinimas { get; set; }

        public string UžduotiesNuoroda { get; set; }

        public DateTime? ĮkelimoData { get; set; }

        public DateTime? PateikimoData { get; set; }

        public DateTime? IkiKadaAtlikti { get; set; }

        public Užduotis()
        {

        }

        public Užduotis(int? dalykoId, int? mokytojoId, string mokytojoVardasPavardė, string pavadinimas, string užduotiesNuoroda, DateTime? pateikimoData, DateTime? ikiKadaAtlikti)
        {
            DalykoId = dalykoId;
            MokytojoId = mokytojoId;
            MokytojoVardasPavardė = mokytojoVardasPavardė;
            Pavadinimas = pavadinimas;
            UžduotiesNuoroda = užduotiesNuoroda;
            ĮkelimoData = !ĮkelimoData.HasValue ? DateTime.Now : ĮkelimoData;
            PateikimoData = pateikimoData;
            IkiKadaAtlikti = ikiKadaAtlikti;
        }
    }
}
